from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import MyUserCreationForm, UserEditForm, ProfileEditForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.views.generic import CreateView, DetailView, View, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.urls import reverse
from django.contrib.auth import get_user_model, update_session_auth_hash
from .models import UserProfile

class LoginView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'accounts/login.html')

    def post(self, request, *args, **kwargs):
        context = {}
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('home_page')
        else:
            context['has_error'] = True
        return render(request, template_name='accounts/login.html', context=context)

class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        next = request.GET.get('next')
        if next:
            return redirect(next)
        return redirect('login')
    
class RegisterView(CreateView):
    model = User
    template_name = 'accounts/register.html'
    form_class = MyUserCreationForm

    def form_valid(self, form):
        user = form.save()
        UserProfile.objects.create(user=user)
        login(self.request, user)
        return redirect(self.get_success_url())
    
    def get_success_url(self):
        next=self.request.GET.get('next')
        if not next:
            next = self.request.POST.get('next')
        return next
    
class UserDetailView(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = 'accounts/user_detail.html'
    context_object_name = 'user_obj'
    paginate_related_by = 5
    paginate_related_orphans = 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        projects = self.object.projects.order_by('-created_at')
        paginator = Paginator(projects, self.paginate_related_by, orphans=self.paginate_related_orphans)
        page_number = self.request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        context['page_obj'] = page
        context['project'] = page.object_list
        context['is_paginated'] = page.has_other_pages()
        return context

class UserEditView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = UserEditForm
    template_name = 'accounts/user_change.html'
    context_object_name = 'user_obj'

    def get_context_data(self, **kwargs):
        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = self.get_profile_form()
        return super().get_context_data(**kwargs)
    
    def get_profile_form(self):
        form_kwargs = {'instance':self.object.profile}
        if self.request.method == 'POST':
            form_kwargs['data'] = self.request.POST
            form_kwargs['files'] = self.request.FILES
        return ProfileEditForm(**form_kwargs)
    
    def get_success_url(self):
        return reverse('user_detail', kwargs={'pk':self.object.pk})
    
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = self.get_profile_form()
        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)
    
    def form_valid(self, form, profile_form):
        response = super().form_valid(form)
        profile_form.save()
        return response
    
    def form_invalid(self, form, profile_form):
        context = self.get_context_data(form=form, profile_form=profile_form)
        return render(context)
    
    def get_object(self, queryset=None):
        return self.request.user

class UserPasswordChangeView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    template_name = 'accounts/password_change.html'
    form_class = PasswordChangeForm
    context_object_name = 'user_obj'

    def form_valid(self, form):
        user = form.save()
        update_session_auth_hash(self.request, user)
        return redirect(self.get_success_url())

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self) -> str:
        return reverse('user_detail', kwargs={'pk': self.object.pk})
