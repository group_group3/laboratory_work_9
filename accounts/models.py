from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group


class UserProfile(models.Model):
    position_choice = (
        ('Junior Developer', 'Junior Developer'),
        ('Senior Developer', 'Senior Developer'),
        ('Lead Developer', 'Lead Developer'),
        ('Team Lead', 'Team Lead'),
        ('Manager', 'Manager'),
    )

    user = models.OneToOneField(
        get_user_model(),
        related_name = 'profile',
        on_delete = models.CASCADE,
        verbose_name = 'User'
        )
    birth_date = models.DateField(null=True, blank=True, verbose_name = 'Birth date')
    position = models.CharField(max_length=100, choices=position_choice, default = 'Junior engineer')
    phone = models.CharField(max_length=15, default = '')
    avatar = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='Avatar')

    def __str__(self):
        return self.user.get_full_name() + "'s profile"

    def save(self, *args, **kwargs):

        group_mapping = {
            'Junior Developer': 'Developers',
            'Senior Developer': 'Developers',
            'Lead Developer': 'Developers',
            'Team Lead': 'Team Leads',
            'Manager': 'Project Managers',
        }
        if self.position in group_mapping:
            group_name = group_mapping[self.position]
            group, _ = Group.objects.get_or_create(name=group_name)
            self.user.groups.add(group)
        super().save(*args, **kwargs)