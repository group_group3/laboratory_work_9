from django.contrib import admin
from .models import UserProfile
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model


class ProfileInline(admin.StackedInline):
    model = UserProfile
    fields = ['birth_date', 'avatar', 'position', 'phone']
    
class ProfileAdmin(UserAdmin):
    inlines = [ProfileInline]

User = get_user_model()
admin.site.unregister(User)
admin.site.register(User, ProfileAdmin)