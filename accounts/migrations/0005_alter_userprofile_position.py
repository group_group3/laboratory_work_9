# Generated by Django 5.0.2 on 2024-03-23 15:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_alter_userprofile_position'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='position',
            field=models.CharField(choices=[('Junior engineer', 'Junior engineer'), ('Senior engineer', 'Senior engineer'), ('Lead engineer', 'Lead engineer'), ('Team lead', 'Team lead'), ('Manager', 'Manager')], default='Junior engineer', max_length=100),
        ),
    ]
