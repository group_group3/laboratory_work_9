from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from projects.models import Project

class Task(models.Model):
    status_choice = (
        ('В ожидании', 'В ожидании'),
        ('В процессе', 'В процессе'),
        ('Выполнено', 'Выполнено'),
    )

    title = models.CharField(max_length=200, null=False, blank=False, verbose_name = 'title')
    description = models.TextField(max_length=3000, null=True, blank=True, verbose_name = 'Text')
    status = models.CharField(max_length=20, choices=status_choice, default='В ожидании')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name = 'Creation date and time')
    users = models.ManyToManyField(User, related_name='tasks', blank=False)
    projects = models.ForeignKey(
        Project, 
        on_delete=models.SET_DEFAULT, 
        default=1,
        null=False, 
        related_name = 'tasks',
        verbose_name='Project')

    def __str__(self):
        return self.title