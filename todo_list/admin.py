from django.contrib import admin
from .models import Task

class TaskAdmin(admin.ModelAdmin):
    list_display = ['title', 'status', 'created_at']
    list_filter = ['status']
    search_fields = ['title', 'description']
    exclude = ['created_at']
    readonly_fields = ['created_at']

admin.site.register(Task, TaskAdmin)
