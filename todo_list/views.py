from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.utils.http import urlencode
from .models import Task
from .forms import SearchForm, TaskForm
from django.db.models import Q
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

class IndexView(TemplateView):
    template_name = 'index.html'

class TaskListView(ListView):
    template_name = 'todo_list/list.html'
    model = Task
    context_object_name = 'tasks'
    form = SearchForm
    paginate_by = 5
    paginate_orphans = 1
    
    def get_queryset(self):
        queryset = super().get_queryset()
        sort_by = self.request.GET.get('sort_by')
        if sort_by:
            queryset = queryset.order_by(sort_by)
        search_value = self.get_search_value()
        if search_value:
            queryset = queryset.filter(title__icontains=search_value)
        return queryset


    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)
    
    def get_search_form(self):
        return self.form(self.request.GET)
    
    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context

class TaskDetailView(DetailView):
    template_name = 'todo_list/detail.html'
    model = Task
    context_object_name = 'task'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        task = self.get_object()
        context['assigned_users'] = task.users.all()
        return context

class TaskCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'todo_list/create.html'
    model = Task
    form_class = TaskForm
    permission_required = ['todo_list.add_task']

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated:
            return redirect('home_page')
        if not user.has_perm('todo_list.add_task'):
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('task_detail', kwargs={'pk':self.object.pk})

class TaskUpdateView(PermissionRequiredMixin, UpdateView):
    model = Task
    template_name = 'todo_list/update.html'
    form_class = TaskForm
    context_object_name = 'task'
    permission_required = ['todo_list.change_task']

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated:
            return redirect('home_page')
        if not user.has_perm('todo_list.change_task'):
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('task_detail', kwargs={'pk': self.object.pk})
    


class TaskDeleteView(PermissionRequiredMixin, DeleteView):
    template_name = 'todo_list/delete.html'
    model = Task
    context_key = 'task'
    success_url = reverse_lazy('task_list')
    permission_required = ['todo_list.delete_task']

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated:
            return redirect('home_page')
        if not user.has_perm('todo_list.delete_task'):
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)