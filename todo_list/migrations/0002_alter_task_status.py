# Generated by Django 5.0.2 on 2024-02-27 16:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo_list', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.CharField(choices=[(0, 'Нужно указать статус'), (1, 'В ожидании'), (2, 'В процессе'), (3, 'Выполнено')], default=0, max_length=20),
        ),
    ]
