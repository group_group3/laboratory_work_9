from django import forms
from django.forms import ValidationError
from .models import Task

class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = '__all__'
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control', 
                'placeholder': 'Title'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 4}),
            'status': forms.Select(attrs={
                'class': 'form-select', 
                'placeholder': 'Status'}),
            'users': forms.SelectMultiple(attrs={'class': 'form-select'}),
            'projects': forms.Select(attrs={'class': 'form-select','placeholder': 'Projects'})
            }
        
    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data.get('description') == cleaned_data.get('title'):
            raise ValidationError("Task title and task description should not be similar!")
        return cleaned_data
    
class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Search')