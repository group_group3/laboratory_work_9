from django.urls import path
from .views import ProjectDetailView, ProjectCreateView, ProjectListView, ProjectUpdateView, ProjectDeleteView

urlpatterns = [
    path('<int:pk>/detail', ProjectDetailView.as_view(), name='project_detail'),
    path('create', ProjectCreateView.as_view(), name='project_create'),
    path('', ProjectListView.as_view(), name='project_list'),
    path('<int:pk>/update', ProjectUpdateView.as_view(), name='project_update'),
    path('<int:pk>/delete', ProjectDeleteView.as_view(), name='project_delete'),
]
