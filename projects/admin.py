from django.contrib import admin
from .models import Project

class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'created_at', 'updated_at']
    list_filter = ['name', 'users']
    search_fields = ['name', 'description']
    exclude = ['created_at']
    readonly_fields = ['created_at', 'updated_at']

admin.site.register(Project, ProjectAdmin)
