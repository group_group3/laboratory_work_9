from django import forms
from django.forms import ValidationError
from .models import Project

class ProjectForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control', 
                'placeholder': 'Name'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 4}),
            'users': forms.SelectMultiple(attrs={'class': 'form-select'})
            }
    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data.get('description') == cleaned_data.get('name'):
            raise ValidationError("Project name and project description should not be similar!")
        return cleaned_data
    
class ProjectSearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Search')