from django.db import models
from django.contrib.auth.models import User
from accounts.models import UserProfile

class Project(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, verbose_name = 'Name')
    description = models.TextField(max_length=3000, null=True, blank=True, verbose_name = 'Text')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name = 'Creation date and time')
    updated_at = models.DateTimeField(auto_now=True, verbose_name = 'Update date and time')
    users = models.ManyToManyField(User, related_name='projects', blank=False)

    def __str__(self):
        return self.name